use image::{codecs::png::PngEncoder, ColorType, Luma};
use js_sys::{Array, Uint8Array};
use qrcode::render::svg;
use qrcode::QrCode;
use web_sys::{Blob, BlobPropertyBag, Url};
use yew::prelude::*;

enum Msg {
    Update(String),
}

struct Model {
    link: ComponentLink<Self>,
    qr_string: String,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            qr_string: String::new(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Update(val) => {
                self.qr_string = val;
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let code = QrCode::new(&self.qr_string.as_bytes()).unwrap();
        let image = code.render::<Luma<u8>>().build();
        let mut x: Vec<u8> = Vec::new();
        let encoder = PngEncoder::new(&mut x);
        encoder
            .encode(image.as_raw(), image.width(), image.height(), ColorType::L8)
            .unwrap();
        let div = yew::utils::document().create_element("div").unwrap();
        div.set_inner_html(&code.render::<svg::Color>().build());
        let array = unsafe { [Uint8Array::view(&*x)].iter().collect::<Array>() };
        let my_blob = Blob::new_with_u8_array_sequence_and_options(
            &array.into(),
            &BlobPropertyBag::new().type_("image/png"),
        )
        .unwrap();
        html! {
            <>
                <h1>{"QR Code Generator"}</h1>
                <label for="contentbox">{"Type the QR code contents in the box"}</label>
                <input type="text" id="contentbox" oninput=self.link.callback(|e: InputData| Msg::Update(e.value))/>
                { Html::VRef(div.into()) }
                <a href=Url::create_object_url_with_blob(&my_blob).unwrap() download="generated_qrcode.png">{"Download QR code"}</a>
            </>
        }
    }
}

fn main() {
    yew::start_app::<Model>();
}
